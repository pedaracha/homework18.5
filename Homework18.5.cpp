﻿#include <iostream>
#include <algorithm>
#include "Player.h"

void initPlayers(Player** players, std::size_t length)
{
    using namespace std;

    for (int i = 0;i < length;i++)
    {
        cout  << "enter the NAME of the " << i << " player: ";
        string name;
        cin >> name;
        cout  << "enter the SCORE of the " << i << " player: ";
        int score;
        cin >> score;
        players[i] = new Player{ name,score };
    }
}

void printPlayers(Player** players, std::size_t length)
{
    using namespace std;

    for (int i = 0;i < length;i++)
    {
        players[i]->Print();
    }
}

void deletePlayers(Player** players, std::size_t length)
{
    using namespace std;

    for (int i = 0;i < length;i++)
    {
        delete players[i];
    }
    delete[] players;
    players = nullptr;
}

bool playerComp(Player* a, Player* b) 
{
    return a->m_score < b->m_score;
}

int main()
{
    using namespace std;

    cout << "how many players: ";
    size_t playerCount;
    cin >> playerCount;

    Player** players = new Player*[playerCount];

    initPlayers(players, playerCount);

    std::sort(players, players + playerCount, playerComp);
    
    printPlayers(players, playerCount);

    deletePlayers(players, playerCount);
}