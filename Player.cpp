#include "Player.h"
#include <iostream>

Player::Player(std::string name, int score) {
	m_playerName = name;
	m_score = score;
}

void Player::Print() {
	std::cout << "player name: " << m_playerName << "\tscore: " << m_score << std::endl;
}