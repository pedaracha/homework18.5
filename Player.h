#pragma once
#include <iostream>

class Player
{
public:
	std::string m_playerName;
	int m_score;
	Player(std::string name, int score);
	void Print();
};

